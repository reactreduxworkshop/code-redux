import { connect } from 'react-redux';
import Filter from './Filter';
import { setFilterValue } from '../../store/data/posts';

const mapDispatchToProps = {
  setFilterValue
};

const mapStateToProps = state => ({
  value: state.posts.filter,
  getValue: () => {
    return state.posts.filter;
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filter);
