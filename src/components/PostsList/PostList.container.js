import { connect } from 'react-redux';
import PostsList from './PostsList';

const mapStateToProps = state => {
  return {
    posts: state.posts.posts,
    filterValue: state.posts.filter
  };
};

export default connect(
  mapStateToProps,
  null
)(PostsList);
