import React from 'react';
import PropTypes from 'prop-types';
import memoizeOne from 'memoize-one';

import './PostsList.css';
import PostItem from '../PostItem/PostItem';
import post from '../../prop-types/post';

const getPosts = (posts, filter) =>
  posts &&
  posts
    .filter(post => post.body.indexOf(filter) > -1)
    .map(post => <PostItem key={post.id} post={post} />);
const getPostsMemoized = memoizeOne((posts, filter) => getPosts(posts, filter));

const PostsList = props => {
  const posts = getPostsMemoized(props.posts, props.filterValue);

  return <div className="posts-list">{posts}</div>;
};

PostsList.defaultProps = {};

PostsList.propTypes = {
  className: PropTypes.string,
  posts: PropTypes.arrayOf(PropTypes.shape(post)),
  filterValue: PropTypes.string.isRequired
};

PostsList.displayName = 'PostsList';

export default PostsList;
