import React, { Component } from 'react';

class Router extends Component {
  static propTypes = {};
  static defaultProps = {};
  static displayName = 'Router';

  constructor(props) {
    super(props);

    this.state = {
      pathname: window.location.pathname
    };
  }

  /*
  it's dirty way to make simple Router
   */

  render() {
    return <div key={this.state.pathname}>{this.props.children}</div>;
  }

  componentDidMount() {
    window.addEventListener('custompushstate', this.updateUrlState);
    window.onpopstate = this.updateUrlState;
  }

  componentWillUnmount() {
    window.removeEventListener('custompushstate', this.updateUrlState);
    window.onpopstate = null;
  }

  updateUrlState = () => {
    this.setState({ pathname: window.location.pathname });
  };
}

export default Router;
