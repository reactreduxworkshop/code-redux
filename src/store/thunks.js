import { getPosts } from '../services/posts';
import { setFilterValue, setPosts } from './data/posts';

export function fetchPosts(userId) {
  return (dispatch, getState) => {
    return getPosts({ userId }).then(data => {
      dispatch(setPosts(data));
      dispatch(setFilterValue(''));
    });
  };
}
