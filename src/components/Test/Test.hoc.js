import { connect } from 'react-redux';

import Test from './Test.js';

const mapStateToProps = (state, ownProps) => {
  return {
    posts: state.posts.posts
  };
};

const TestHOC = connect(mapStateToProps)(Test);
TestHOC.displayName = 'TestHOC';

export default TestHOC;
