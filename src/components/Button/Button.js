import React from 'react';
import PropTypes from 'prop-types';

import './Button.css';

const Button = props => {
  let onClick = props.onClick;
  if (props.type === 'submit') {
    onClick = null;
  }

  return (
    <button type={props.type} onClick={onClick} disabled={props.disabled}>
      {props.children}
    </button>
  );
};

Button.defaultProps = {};

Button.propTypes = {
  children: PropTypes.node,
  type: PropTypes.oneOf(['submit']),
  onClick: PropTypes.func, // TODO should be forced when type is not submit
  disabled: PropTypes.bool
};

Button.displayName = 'Button';

export default Button;
