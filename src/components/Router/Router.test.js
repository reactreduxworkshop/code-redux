import React from 'react';

import { shallow } from 'enzyme';
import Router from './Router';

describe('Router', () => {
  const wrapper = shallow(<Router>Some</Router>);

  beforeEach(() => {
    window.history.pushState({}, 'Test Page', '/');
  });

  it('should render children', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should read and set pathname on custompushstate event', () => {
    window.history.pushState({}, 'Test Page', '/new');
    window.dispatchEvent(new Event('custompushstate'));
    expect(wrapper.state().pathname).toBe('/new');
  });

  it('should call addEventListener opn mount', () => {
    window.addEventListener = jest.fn();
    window.removeEventListener = jest.fn();

    const wrapper = shallow(<Router>Some</Router>);
    const updateUrlState = wrapper.instance().updateUrlState; // instance is null after unmount() so we have to store it

    expect(window.addEventListener).toHaveBeenCalled();
    expect(window.addEventListener).toHaveBeenCalledWith(
      'custompushstate',
      updateUrlState
    );

    wrapper.unmount();
    expect(window.removeEventListener).toHaveBeenCalled();
    expect(window.removeEventListener).toHaveBeenCalledWith(
      'custompushstate',
      updateUrlState
    );

    window.addEventListener.mockRestore();
    window.removeEventListener.mockRestore();
  });
});
