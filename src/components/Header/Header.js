import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Header.css';
import SearchPanel from '../SearchPanel';

const Header = props => {
  // Keep in mind that SearchPanel can be passed as child to Header
  // it's to 'simulate' deep nesting
  return (
    <div className={classNames('header', props.className)}>
      <SearchPanel />
    </div>
  );
};

Header.defaultProps = {};

Header.propTypes = {
  className: PropTypes.string
};

Header.displayName = 'Header';

export default Header;
