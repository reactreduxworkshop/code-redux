import fetcher, { responseDataHandler } from './fetcher';
import { API } from '../constants';

export function getPosts(params) {
  const query = Object.keys(params).map(
    paramName => `${paramName}=${params[paramName]}`
  );

  return fetcher(`${API}/posts?${query.join('&')}`).then(responseDataHandler);
}
