import React from 'react';
import PropTypes from 'prop-types';

import './PostItem.css';
import post from '../../prop-types/post';

const PostItem = props => {
  const postData = Object.keys(props.post).map(postProp => (
    <div key={postProp}>
      {postProp}: {props.post[postProp]}
    </div>
  ));
  const goToPost = id => {
    window.history.pushState(null, '', `/post?id=${id}`);
    window.dispatchEvent(new Event('custompushstate'));
  };

  return (
    <div className="post-item" onClick={() => goToPost(props.post.id)}>
      {postData}
    </div>
  );
};

PostItem.defaultProps = {};

PostItem.propTypes = {
  className: PropTypes.string,
  post: PropTypes.shape(post)
};

PostItem.displayName = 'PostItem';

export default PostItem;
