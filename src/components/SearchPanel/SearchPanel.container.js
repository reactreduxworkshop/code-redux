import { connect } from 'react-redux';
import { setUserId } from '../../store/data/posts';
import { fetchPosts } from '../../store/thunks';
import SearchPanel from './SearchPanel';

const mapDispatchToProps = {
  fetchPosts,
  setUserId
};

const mapStateToProps = state => ({
  defaultUserId: state.posts.userId
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchPanel);
