import React from 'react';
import PropTypes from 'prop-types';

import './Filter.css';
import Input from '../Input';

const Filter = props => {
  return (
    <div className="filter">
      <Input
        onChange={props.setFilterValue}
        value={props.value}
        placeholder="search for body"
      />
    </div>
  );
};

Filter.defaultProps = {};

Filter.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired
};

Filter.displayName = 'Filter';

export default Filter;
