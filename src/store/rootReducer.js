import { combineReducers } from 'redux';

import posts from './data/posts';

const rootReducer = combineReducers({
  posts
});

export default rootReducer;
