import React, { Component } from 'react';

import './App.css';
import Router from '../Router';
import Route from '../Route';
import SearchView from '../SearchView';

class App extends Component {
  static propTypes = {};
  static defaultProps = {};
  static displayName = 'App';

  render() {
    return (
      <div className="app">
        <Router>
          <Route url="/">
            <SearchView />
          </Route>
          <Route url="/post">
            <button onClick={() => window.history.back()}>Back</button>
            Single post mock
          </Route>
        </Router>
      </div>
    );
  }
}

export default App;
