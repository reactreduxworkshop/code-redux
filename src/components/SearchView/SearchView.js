import React, { Component } from 'react';

import Header from '../Header';
import Content from '../Content';

class SearchView extends Component {
  static propTypes = {};
  static defaultProps = {};
  static displayName = 'SearchView';

  render() {
    return (
      <div className="search-view">
        <Header />
        <Content />
      </div>
    );
  }
}

export default SearchView;
