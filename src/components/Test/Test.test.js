import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import logger from '../../store/middlewares/logger';

import { getPosts } from './Test';
import { SET_POSTS } from '../../store/actionNames';
import Test from './Test.hoc';

describe('Test component', () => {
  let store;
  const middlewares = [thunk, logger];
  const mockStore = configureMockStore(middlewares);

  afterEach(() => {
    // when you reuse store across tests to clear dispatched actions:
    if (store) {
      store.clearActions();
    }
  });

  test('should get posts from store', () => {
    const state = {
      posts: {
        posts: [{ id: 2 }]
      }
    };
    const store = mockStore(state);

    const expectedActions = [{ type: SET_POSTS, payload: [{ id: 3 }] }];
    return store.dispatch(getPosts()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  test('should render Test', () => {
    const state = {
      posts: {
        posts: [{ id: 2 }]
      }
    };
    const store = mockStore(state);

    const wrapper = mount(
      <Provider store={store}>
        <Test />
      </Provider>
    );

    expect(wrapper).toMatchSnapshot();
  });
});
