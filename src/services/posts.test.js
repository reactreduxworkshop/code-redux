import fetchMock from 'fetch-mock';

import { API } from '../constants';
import { getPosts } from './posts';

const mockResponse = [{ id: '1' }, { id: '2' }];
describe('posts service', () => {
  afterEach(() => {
    fetchMock.restore();
  });

  it('should call posts api with query', () => {
    fetchMock.mock(`${API}/posts?userId=3&costam=4`, mockResponse);

    expect.assertions(2);
    return getPosts({ userId: 3, costam: 4 }).then(data => {
      expect(data).toEqual(mockResponse);
      expect(fetchMock.calls(true).length).toBe(1);
    });
  });

  it('should call posts api with query - 404', () => {
    fetchMock.mock(`${API}/posts?userId=3&costam=4`, 404);

    expect.assertions(2);
    return getPosts({ userId: 3, costam: 4 }).catch(err => {
      expect(fetchMock.calls(true).length).toBe(1);
      expect(err).toBe('unknown response');
    });
  });
});
