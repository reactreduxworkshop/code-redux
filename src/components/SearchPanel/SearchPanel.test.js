import React from 'react';

import { shallow, mount } from 'enzyme';
import SearchPanel, { STATES } from './SearchPanel';

const mockedData = [
  {
    id: 1
  }
];
jest.mock('../../services/posts', () => {
  return {
    getPosts: jest.fn(() => Promise.resolve(mockedData))
  };
});

describe('SearchPanel', () => {
  let orgDate;
  let dateStamp = 1543010841346;
  beforeAll(() => {
    orgDate = Date.now;
    Date.now = () => dateStamp;
  });

  afterAll(() => {
    Date.now = orgDate;
  });

  const props = {
    onPostsResults: jest.fn()
  };
  const wrapper = shallow(<SearchPanel {...props} />);
  const initialState = wrapper.state();

  it('Should render', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should update userId in state when userId is number', () => {
    wrapper.setState(initialState);
    expect(wrapper.state().userId).toBe('');
    wrapper.instance().handleUserIdChange('1');
    expect(wrapper.state().userId).toBe('1');
  });

  it('should not update userId in state when userId is not number', () => {
    wrapper.setState(initialState);
    expect(wrapper.state().userId).toBe('');
    wrapper.instance().handleUserIdChange('a');
    expect(wrapper.state().userId).toBe('');
  });

  it('should call passed method in onPostsResults after successful fetch', () => {
    expect.assertions(2);
    return wrapper
      .instance()
      .fetchPosts('2')
      .then(() => {
        expect(props.onPostsResults).toHaveBeenCalledTimes(1);
        expect(props.onPostsResults).toHaveBeenCalledWith(mockedData);
      });
  });

  it('should set state to fetching during fetching', () => {
    const e = {
      preventDefault: jest.fn()
    };

    wrapper.instance().setState = jest.fn();

    expect.assertions(5);
    expect(wrapper.state().state).toBe(STATES.IDLE);

    return wrapper
      .instance()
      .handleSubmit(e)
      .then(() => {
        expect(wrapper.state().state).toBe(STATES.IDLE);

        expect(e.preventDefault).toHaveBeenCalled();
        expect(wrapper.instance().setState).toHaveBeenCalledTimes(2);
        expect(wrapper.instance().setState.mock.calls).toEqual([
          [{ state: STATES.FETCHING }],
          [{ lastSearch: dateStamp, state: STATES.IDLE }]
        ]);
      })
      .then(() => {
        wrapper.instance().setState.mockRestore();
      })
      .catch(() => {
        wrapper.instance().setState.mockRestore();
      });
  });
});
