import React from 'react';
import PropTypes from 'prop-types';

import './Input.css';

const Input = props => {
  return (
    <input
      className="input"
      value={props.value}
      onChange={e => props.onChange(e.target.value)}
      placeholder={props.placeholder}
      disabled={props.disabled}
    />
  );
};

Input.defaultProps = {};

Input.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  disabled: PropTypes.bool
};

Input.displayName = 'Input';

export default Input;
