import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './SearchPanel.css';
import Input from '../Input';
import Button from '../Button';
import Panel from '../Panel';

export const STATES = {
  FETCHING: 'FETCHING',
  IDLE: 'IDLE'
};

class SearchPanel extends Component {
  static defaultProps = {
    className: PropTypes.string
  };
  static displayName = 'SearchPanel';

  constructor(props) {
    super(props);

    this.state = {
      userId: props.defaultUserId,
      state: STATES.IDLE,
      lastSearch: 'nigdy'
    };
  }

  render() {
    return (
      <Panel className="search-panel">
        <form onSubmit={(...args) => this.handleSubmit(...args)}>
          <Input
            onChange={this.handleUserIdChange}
            value={this.state.userId}
            placeholder="id użytkownika"
            disabled={this.state.state === STATES.FETCHING}
          />
          <Button type="submit" disabled={this.state.state === STATES.FETCHING}>
            Szukaj (ostatnio: {this.state.lastSearch})
          </Button>
          {this.state.state === STATES.FETCHING ? 'Loading...' : null}
        </form>
      </Panel>
    );
  }

  handleUserIdChange = userId => {
    if (isNaN(userId)) {
      return;
    }

    this.setState({ userId });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.setUserId(this.state.userId);
    this.setState({ state: STATES.FETCHING });
    return this.props
      .fetchPosts(this.state.userId)
      .then(() =>
        this.setState({ state: STATES.IDLE, lastSearch: Date.now() })
      );
  };
}

export default SearchPanel;
