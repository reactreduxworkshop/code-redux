import React from 'react';
import { shallow } from 'enzyme';

import Button from './Button';

describe('Button', () => {
  describe('When type is submit', () => {
    const props = {
      type: 'submit',
      onClick: jest.fn(),
      disabled: false
    };

    const wrapper = shallow(<Button {...props}>Klikaj</Button>);

    it('should render', () => {
      expect(wrapper).toMatchSnapshot();
    });

    it('should set onClick to when type is submit', () => {
      expect(wrapper.find('button').props().onClick).toBe(null);
    });

    it('should not call onClick on click when type is submit', () => {
      wrapper.find('button').simulate('click');
      expect(props.onClick).not.toHaveBeenCalled();
    });
  });

  describe('When type is not submit', () => {
    const props = {
      type: null,
      onClick: jest.fn(),
      disabled: false
    };

    const wrapper = shallow(<Button {...props}>Klikaj</Button>);

    it('should render', () => {
      expect(wrapper).toMatchSnapshot();
    });

    it('should set onClick to when type is submit', () => {
      expect(wrapper.find('button').props().onClick).toBe(props.onClick);
    });

    it('should not call onClick on click when type is submit', () => {
      wrapper.find('button').simulate('click');
      expect(props.onClick).toHaveBeenCalledTimes(1);
    });
  });
});
