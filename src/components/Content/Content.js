import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Content.css';
import Panel from '../Panel';
import Filter from '../Filter';
import PostsList from '../PostsList/';
import post from '../../prop-types/post';

class Content extends Component {
  static defaultProps = {};
  static displayName = 'Content';

  render() {
    // Keep in mind that PostsList can be passed as child to Content
    // it's to 'simulate' deep nesting
    return (
      <Panel className="header">
        <Filter />
        <PostsList />
      </Panel>
    );
  }
}

export default Content;
