function fetcher(...args) {
  return fetch(...args);
}

export function responseDataHandler(response) {
  // TODO should check and handle 204 (no content), text responses etc
  if (response.status !== 200) {
    return Promise.reject('unknown response');
  }

  return response.json();
}

export default fetcher;
