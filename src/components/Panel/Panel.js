import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import './Panel.css';

const Panel = props => {
  return (
    <div className={classNames('panel', props.className)}>{props.children}</div>
  );
};

Panel.defaultProps = {};

Panel.propTypes = { className: PropTypes.string };

Panel.displayName = 'Panel';

export default Panel;
