import PropTypes from 'prop-types';

const Route = props => {
  if (window.location.pathname !== props.url) {
    return null;
  }

  return props.children;
};

Route.defaultProps = {};

Route.propTypes = {
  url: PropTypes.string.isRequired
};

Route.displayName = 'Route';

export default Route;
