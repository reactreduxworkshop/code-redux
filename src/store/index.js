import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';

import rootReducer from './rootReducer';
import logger from './middlewares/logger';

const composeEnhancers =
  process.env.NODE_ENV === 'production'
    ? compose
    : composeWithDevTools({
        // Specify name here, actionsBlacklist, actionsCreators and other options if needed
      });

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk, logger))
);

// without Redux DevTools:
// const store = createStore(
//   rootReducer,
//   applyMiddleware(thunk, logger)
// );

export default store;
