import { createAction, handleActions } from 'redux-actions';

import { SET_FILTER_VALUE, SET_POSTS, SET_USER_ID } from '../actionNames';

/*
Actions
 */
export const setPosts = createAction(SET_POSTS);
export const setUserId = createAction(SET_USER_ID);
export const setFilterValue = createAction(SET_FILTER_VALUE);

/*
Reducer
 */
const defaultState = {
  posts: null,
  userId: '',
  filter: ''
};
export default handleActions(
  {
    [SET_POSTS]: (state, action) => {
      return { ...state, posts: action.payload };
    },
    [SET_USER_ID]: (state, action) => {
      return { ...state, userId: action.payload };
    },
    [SET_FILTER_VALUE]: (state, action) => {
      return { ...state, filter: action.payload };
    }
  },
  defaultState
);

/*
Selectors
 */
