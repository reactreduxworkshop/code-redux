import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Test.css';
import { setPosts } from '../../store/data/posts';

const Test = props => {
  return (
    <div className={classNames('test', props.className)}>
      {JSON.stringify(props.posts)}
    </div>
  );
};

export const getPosts = () => {
  return (dispatch, getState) => {
    return Promise.resolve().then(() => {
      dispatch(setPosts([{ id: 3 }]));
    });
  };
};

Test.defaultProps = {};

Test.propTypes = {
  className: PropTypes.string
};

Test.displayName = 'Test';

export default Test;
