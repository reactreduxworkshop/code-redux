const logger = store => next => action => {
  if (action.type === 'NOT') {
    // swallow action
    return store.getState();
  }

  if (action.type === 'DUP') {
    store.dispatch({ type: 'DUP2' });
  }

  const nextStore = next(action);
  console.log(action, store.getState());
  return nextStore;
};

export default logger;
